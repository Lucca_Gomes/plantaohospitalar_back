const Erro = require("./Erro");

class ErroSessao extends Erro {
    constructor(params) {
        params.erro = "ErroParametroDeRequisicao";
        super(params);
    }
}

class ErroEmailJaCadastrado extends ErroSessao {
    constructor({ email }) {
        super({
            tipo: "ErroEmailJaCadastrado",
            descricao: `O email ${email} já foi cadastrado.`,
            info: {
                email: email
            }
        });
    }
}

class ErroLoginInvalido extends ErroSessao {
    constructor({ email, senha }) {
        super({
            tipo: "ErroLoginInvalido",
            descricao: "Informações de login incorretas e/ou não compatíveis.",
            info: {
                email: email,
                senha: senha
            }
        });
    }
}

class ErroUsuarioNaoAprovado extends ErroSessao {
    constructor() {
        super({
            tipo: "ErroUsuarioNaoAprovado",
            descricao: "O usuário ainda não foi aprovado por um administrador.",
        });
    }
}

class ErroTokenExpirado extends ErroSessao {
    constructor() {
        super({
            tipo: "ErroTokenExpirado",
            descricao: "Token expirado.",
        });
    }
}

class ErroTokenAssinaturaInvalida extends ErroSessao {
    constructor() {
        super({
            tipo: "ErroTokenAssinaturaInvalida",
            descricao: "Token e/ou secretKey inválidos ou malformados.",
        });
    }
}

class ErroAutorizacaoInsuficiente extends ErroSessao {
    constructor({ authAtual, authNecessaria }) {
        super({
            tipo: "ErroAutorizacaoInsuficiente",
            descricao: `Necessário permissão de acesso '${authNecessaria}', nível de acesso atual '${authAtual}'.`,
        });
    }
}

module.exports = {
    ErroSessao,
    ErroEmailJaCadastrado,
    ErroLoginInvalido,
    ErroUsuarioNaoAprovado,
    ErroTokenExpirado,
    ErroTokenAssinaturaInvalida,
    ErroAutorizacaoInsuficiente
}
