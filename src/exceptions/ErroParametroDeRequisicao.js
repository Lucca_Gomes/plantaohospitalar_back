const Erro = require("./Erro");

class ErroParametroDeRequisicao extends Erro {
    constructor(params) {
        params.erro = "ErroParametroDeRequisicao";
        super(params);
    }
}

class ErroParametroObrigatorio extends ErroParametroDeRequisicao {
    constructor({ paramNome, paramIn }) {
        super({
            tipo: "ErroParametroObrigatorio",
            descricao: `O parametro obrigatório '${paramNome}' foi encontrada(o) com valor null.`,
            info: {
                paramNome: paramNome,
                paramIn: paramIn
            }
        });
    }
}

class ErroParametroValor extends ErroParametroDeRequisicao {
    constructor({ paramNome, paramIn, paramTipo, paramTipoEsperado }) {
        super({
            tipo: "ErroParametroValor",
            descricao: `O parametro '${paramNome}' foi encontrado com valor do tipo ${paramTipo}, esperado ${paramTipoEsperado}.`,
            info: {
                paramNome: paramNome,
                paramIn: paramIn,
                paramTipo: paramTipo,
                paramTipoEsperado: paramTipoEsperado
            }
        });
    }
}

class ErroParametroFormato extends ErroParametroDeRequisicao {
    constructor({ paramNome, paramIn, paramFormato }) {
        super({
            tipo: "ErroParametroFormato",
            descricao: `O parametro '${paramNome}' foi encontrado com formato desconhecido, esperado: ${paramFormato}.`,
            info: {
                paramNome: paramNome,
                paramIn: paramIn
            }
        });
    }
}

module.exports = {
    ErroParametroDeRequisicao,
    ErroParametroObrigatorio,
    ErroParametroValor,
    ErroParametroFormato
}
