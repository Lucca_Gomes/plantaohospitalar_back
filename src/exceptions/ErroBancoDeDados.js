const Erro = require("./Erro");

class ErroBancoDeDados extends Erro {
    constructor(params) {
        params.erro = "ErroBancoDeDados";

        super(params);
    }
}

class ErroNaoEncontrado extends ErroBancoDeDados {
    constructor({ tabela }) {
        super({
            tipo: "ErroNaoEncontrado",
            descricao: `Informação da tabela ${tabela} não encontrada no banco de dados.`,
        });
    }
}


module.exports = {
    ErroBancoDeDados,
    ErroNaoEncontrado
}
