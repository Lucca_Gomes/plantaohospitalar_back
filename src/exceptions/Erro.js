class Erro {
    constructor({ erro="Erro", tipo="ErroGenerico", descricao="Ocorreu um erro.", info={} }) {
        this.erro = erro;
        this.tipo = tipo;
        this.descricao = descricao;
        this.info = info;
    }
}

module.exports = Erro
