const Erro = require("./Erro");

class ErroCorpoDeRequisicao extends Erro {
    constructor(params) {
        params.erro = "ErroCorpoDeRequisicao";
        super(params);
    }
}

class ErroCorpoDeRequisicaoVazio extends ErroCorpoDeRequisicao {
    constructor() {
        super({
            tipo: "ErroCorpoDeRequisicaoVazio",
            descricao: "O corpo de requisição foi encontrado vazio.",
        });
    }
}

class ErroPropriedadeObrigatoria extends ErroCorpoDeRequisicao {
    constructor({ propNome }) {
        super({
            tipo: "ErroPropriedadeObrigatoria",
            descricao: `Propriedade obrigatória do corpo de requisição '${propNome}' foi encontrada(o) com valor null.`,
            info: {
                propNome: propNome
            }
        });
    }
}

class ErroPropriedadeValor extends ErroCorpoDeRequisicao {
    constructor({ propNome, propTipo, propTipoEsperado }) {
        super({
            tipo: "ErroPropriedadeValor",
            descricao: `A propriedade '${propNome}' foi encontrada com valor do tipo ${propTipo}, esperado ${propTipoEsperado}.`,
            info: {
                propNome: propNome,
                propTipo: propTipo,
                propTipoEsperado: propTipoEsperado
            }
        });
    }
}

module.exports = {
    ErroCorpoDeRequisicao,
    ErroCorpoDeRequisicaoVazio,
    ErroPropriedadeObrigatoria,
    ErroPropriedadeValor
}
