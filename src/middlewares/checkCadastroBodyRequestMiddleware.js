const erroCorpoDeRequisicao = require("../exceptions/ErroCorpoDeRequisicao");
const erroSessao = require("../exceptions/ErroSessao");
const { Usuario } = require("../models/models");


async function checkCadastroBodyRequest(req, res, next) {
    const dados = req.body;

    req.dados = {};

    reqBodyPattern = {
        0: ["tipo", "nome", "email", "senha"],
        1: [],
        2: ["crm_cnpj", "endereco"],
        3: ["crm_cnpj", "cpf", "areaDeAtuacaoId"]
    }

    for (let prop of reqBodyPattern[0]) {
        if (dados[prop] == null) {
            return res.status(400).json(
                new erroCorpoDeRequisicao.ErroPropriedadeObrigatoria({ propNome: prop })
            )
        }

        req.dados[prop] = dados[prop];
    }

    for (let prop of reqBodyPattern[dados.tipo]) {
        if (dados[prop] == null) {
            return res.status(400).json(
                new erroCorpoDeRequisicao.ErroPropriedadeObrigatoria({ propNome: prop })
            )
        }

        req.dados[prop] = dados[prop];
    }

    const usuario = await Usuario.findOne({ where: { email: req.dados.email } });

    if (usuario) {
        return res.status(400).json(
            new erroSessao.ErroEmailJaCadastrado({ email: req.dados.email })
        );
    }

    return next()
}

module.exports = checkCadastroBodyRequest;
