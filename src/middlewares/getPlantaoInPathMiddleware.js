const { Plantao } = require("../models/models");

const Erro = require("../exceptions/Erro");
const erroBancoDeDados = require("../exceptions/ErroBancoDeDados");

async function getPlantaoInPath(req, res, next) {
    try {
        const { id } = req.params;

        const plantao = await Plantao.findOne({
            where: {
                id
            }
        })

        if (plantao == null) {
            return res.status(404).json(
                new erroBancoDeDados.ErroNaoEncontrado({ tabela: "Plantoes" })
            )
        }

        req.plantao = plantao;

        return next();

    } catch (error) {
        return res.status(400).json(new Erro({ descricao: "Ocorreu uma falha ao tentar ler o parâmetro id do plantão." }))
    }
}

module.exports = getPlantaoInPath;
