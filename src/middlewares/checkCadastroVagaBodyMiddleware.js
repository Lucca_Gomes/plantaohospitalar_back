async function  checkCadastroVagaBody(req, res, next) {
    try {
        const dados = req.body;
        console.log(dados);
        const vagaBody = ["inicio", "fim", "valor", "areaDeAtuacaoId", "hospitalId"];

        for(let prop of vagaBody) {
            if(dados[prop] == null) {
                return res.status(400).json({
                    erro: "erroCorpoDeRequisicao",
                    erroTipo: "propriedadeObrigatoria",
                    erroDescricao: `Propriedade obrigatória do corpo de requisição '${prop}' foi encontrada(o) com valor null.`,
                    erroInfo: {
                        propriedade: prop
                    }
                });
            }
        }


        if (typeof dados['hospitalId'] !== "string") {
            return res.status(400).json({
                erro: "erroCorpoDeRequisicao",
                erroTipo: "tipoErrado",
                erroDescricao: `Propriedade 'hospitalId' deve ser do tipo String.`,
                erroInfo: {
                    propriedade: 'hospitalId'
                }
            });
        }

        req.dados = dados;
        return next();
    } catch (error) {
        return res.status(400).json({
            erro: "erro",
            erroTipo: "erro",
            erroDescricao: "Ocorreu um erro ao verificar o cadastro da vaga",
        });
    }
}

module.exports = checkCadastroVagaBody;