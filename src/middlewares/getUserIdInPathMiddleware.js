const { Usuario } = require("../models/models");

const Erro = require("../exceptions/Erro");
const erroParametroDeRequisicao = require("../exceptions/ErroParametroDeRequisicao");
const erroBancoDeDados = require("../exceptions/ErroBancoDeDados");

async function getUserInPath(req, res, next) {
    try {
        const { id } = req.params;

        if (id.length != 4 || isNaN(id)) {
            return res.status(400).json(
                new erroParametroDeRequisicao.ErroParametroFormato({
                    paramNome: "id",
                    paramIn: "path",
                    paramFormato: "string numérica com 4 digitos"
                })
            )
        }

        const usuario = await Usuario.findOne({
            where: {
                id
            }
        })

        if (usuario == null) {
            return res.status(404).json(
                new erroBancoDeDados.ErroNaoEncontrado({ tabela: "Usuarios" })
            )
        }

        req.usuario = usuario;

        return next();

    } catch (error) {
        return res.status(400).json(
            new Erro({ descricao: "Ocorreu uma falha ao tentar ler o parâmetro id do usuário." })
        )
    }
}

module.exports = getUserInPath;
