const { Usuario, TipoUsuario } = require("../models/models");

const jwt = require('jsonwebtoken');

const erroParametroDeRequisicao = require("../exceptions/ErroParametroDeRequisicao");
const erroSessao = require("../exceptions/ErroSessao");
const Erro = require("../exceptions/Erro");


async function checkAdminAuth(req, res, next) {
    try {
        const { adminToken, adminSecretKey } = req.query;

        if (adminToken == null) {
            return res.status(401).json(
                new erroParametroDeRequisicao.ErroParametroObrigatorio({
                    paramNome: "adminToken",
                    paramIn: "query"
                })
            )
        }

        if (adminSecretKey == null) {
            return res.status(401).json(
                new erroParametroDeRequisicao.ErroParametroObrigatorio({
                    paramNome: "adminSecretKey",
                    paramIn: "query"
                })
            )
        }

        const decoded = jwt.verify(adminToken, adminSecretKey);
        const usuarioId = decoded.id;

        const usuario = await Usuario.findByPk(usuarioId);

        if (usuario == null) {
            return res.status(401).json(new erroSessao.ErroTokenAssinaturaInvalida())
        }

        const tipoUsuario = await TipoUsuario.findByPk(usuario.tipo);
        const tipo = tipoUsuario.tipo;

        if (tipo != "admin") {
            return res.status(403).json(
                new erroSessao.ErroAutorizacaoInsuficiente({ authAtual: tipo, authNecessaria: "admin" })
            )
        }

        return next();

    } catch (error) {
        console.log(error.name)
        switch (error.name) {
            case "TokenExpiredError":
                return res.status(401).json(new erroSessao.ErroTokenExpirado());
            case "JsonWebTokenError":
                return res.status(401).json(new erroSessao.ErroTokenAssinaturaInvalida());
        }

        return res.status(401).json(new Erro({ descricao: "Ocorreu uma falha ao tentar verificar o nível de autorização." }));
    }
}

module.exports = checkAdminAuth;
