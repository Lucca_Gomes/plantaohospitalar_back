const Vaga = require("../models/vaga")

async function getVagaById(req, res, next) {
    try {
        const { id } = req.params;
        const vaga = await Vaga.findByPk( id );
        if(!vaga){
            return res.status(404).json({
                erro: "erroBancoDeDados",
                erroTipo: "vagaNaoEncontrada",
                erroDescricao: `Vaga de id '${id} não encontrada no banco de dados.'`,
            });
        }

        req.vaga = vaga;
        return next();
        
    } catch (error) {
        return res.status(400).json({
            erro: "erro",
            erroTipo: "erro",
            erroDescricao: "Ocorreu uma falha ao tentar ler o parâmetro id da vaga."
        })
    }
}

module.exports = getVagaById;