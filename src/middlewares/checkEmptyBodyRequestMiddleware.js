const Erro = require("../exceptions/Erro");
const erroCorpoDeRequisicao = require("../exceptions/ErroCorpoDeRequisicao");

async function checkEmptyBodyRequest(req, res, next) {
    try {
        if (Object.keys(req.body).length === 0) {
            return res.status(400).json(
                new erroCorpoDeRequisicao.ErroCorpoDeRequisicaoVazio()
            );
        }

        return next();

    } catch (error) {
        return res.status(400).json(
            new Erro({ descricao: "Ocorreu uma falha ao tentar ler o corpo da requisição." })
        )
    }
}

module.exports = checkEmptyBodyRequest;
