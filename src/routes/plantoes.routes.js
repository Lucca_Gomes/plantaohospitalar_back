const express = require('express');
const plantaoRoutes = express.Router();

const plantoesController = require('../controllers/plantoesController');

const checkEmptyBodyRequest = require("../middlewares/checkEmptyBodyRequestMiddleware");

const getPlantaoInPath = require("../middlewares/getPlantaoInPathMiddleware");
const getUserInPath = require("../middlewares/getUserIdInPathMiddleware");

plantaoRoutes.get("/", (req, res) => plantoesController.listaPlantoes(req, res));
plantaoRoutes.post("/", checkEmptyBodyRequest, (req, res) => plantoesController.criaPlantao(req, res));

plantaoRoutes.get("/:id", getPlantaoInPath, (req, res) => plantoesController.getPlantaoById(req, res));
plantaoRoutes.patch("/:id", checkEmptyBodyRequest, getPlantaoInPath, (req, res) => plantoesController.patchPlantaoById(req, res));
plantaoRoutes.delete("/:id", getPlantaoInPath, (req, res) => plantoesController.deletePlantaoById(req, res));

plantaoRoutes.get("/usuario/:id", getUserInPath, (req, res) => plantoesController.getPlantoesByUser(req, res));

module.exports = plantaoRoutes;