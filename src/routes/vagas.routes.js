const express = require("express");
const vagasRoutes = express.Router();

// adicionando o controller
const vagasController = require("../controllers/vagasController");

//adicionando os Middlewares
const getVagaById = require("../middlewares/getVagaByIdMiddleware");
const checkEmptyBodyRequest = require("../middlewares/checkEmptyBodyRequestMiddleware");
const checkCadastroVagaBody = require("../middlewares/checkCadastroVagaBodyMiddleware");
//Criar nova vaga
vagasRoutes.post("/", checkCadastroVagaBody, (req, res) => vagasController.criarVaga(req, res));

//listar todas as vagas
vagasRoutes.get("/", (req, res) => vagasController.listarVagas(req, res));

//Retornar uma vaga pelo seu id
vagasRoutes.get("/:id", getVagaById, (req, res) => vagasController.retornaVaga(req, res));

//Atualizar informações da vaga
vagasRoutes.patch("/:id", checkEmptyBodyRequest, getVagaById, (req, res) => vagasController.atualizaVaga(req, res));

//Excluir uma Vaga
vagasRoutes.delete("/:id", getVagaById, (req, res) => vagasController.excluiVaga(req, res));

//Retorna vagas de um Hospital
vagasRoutes.get("/hospital/:hospitalId", (req, res) => vagasController.retornaVagasHospital(req, res));

//Retorna vagas por area de atuação
vagasRoutes.get("/area-de-atuacao/:areaDeAtuacaoId", (req, res) => vagasController.retornaVagasArea(req, res));

module.exports = vagasRoutes;