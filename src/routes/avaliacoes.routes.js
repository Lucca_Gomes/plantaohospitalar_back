const express = require('express')
const route = express.Router()
const avaliacoesController = require('../controllers/avaliacoesController')

// lista de avaliações
route.get('/', (req, res)=>{
    avaliacoesController.listaAvaliacoes(req, res);
})

//Enviar avaliação
route.post('/', (req, res)=>{
    avaliacoesController.enviaAvaliacao(req, res);
})

//Encontrar avaliação por ID
route.get('/:id', (req, res) => {
    avaliacoesController.avaliacaoId(req,res);
})

// avaliacoes/de/{usuarioId} //
route.get('/de/:usuarioId', (req, res)=>{
    avaliacoesController.avaliacoesDeUser(req, res)
})


//avaliacoes/para/{usuarioId}//
route.get('/para/:usuarioId', (req, res)=>{
    avaliacoesController.avaliacoesParaUser(req, res)
})

//avaliacoes/media/{usuarioId}//
route.get('/media/:usuarioId', (req, res)=>{
    avaliacoesController.avaliacoesMedia(req, res)
})

module.exports = route