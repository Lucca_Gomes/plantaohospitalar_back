const express = require('express');
const candidatoRoutes = express.Router();

const candidatosController = require('../controllers/candidatosController');

const checkEmptyBodyRequest = require("../middlewares/checkEmptyBodyRequestMiddleware");
const getUserIdInPath = require("../middlewares/getUserIdInPathMiddleware");

candidatoRoutes.get("/", (req, res) => candidatosController.listaCandidatos(req, res));
candidatoRoutes.post("/", checkEmptyBodyRequest, (req, res) => candidatosController.criaCandidato(req, res));

candidatoRoutes.get("/:id", (req, res) => candidatosController.getCandidatoById(req, res));

candidatoRoutes.get("/medico/:id", getUserIdInPath, (req, res) => candidatosController.getCandidatosByMedico(req, res));
//candidatoRoutes.get("/vaga/:id", (req, res) => candidatosController.getCandidatosByVaga(req, res));

module.exports = candidatoRoutes;