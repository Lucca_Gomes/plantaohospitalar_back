const express = require('express')
const route = express.Router()
const areaAtuacaocontroller = require('../controllers/areaAtuacaocontroller')


route.post('/', (req, res)=>{

    areaAtuacaocontroller.createareaAtuacao(req, res)

})

route.get('/', (req, res)=>{

    areaAtuacaocontroller.retornaLista(req, res)

})


route.get('/:id', (req, res)=>{
    
    areaAtuacaocontroller.retornaId(req, res)

})

module.exports = route