const Router = require('express');


const tiposDeUsuariosRoutes = require("./tiposDeUsuarios.routes");
const areasAtuacaoRoutes = require("./areasDeAtuacao.routes");
const usuariosRoutes = require("./usuarios.routes");
const plantoesRoutes = require("./plantoes.routes");
const candidatosRoutes = require("./candidatos.routes");
const avaliacaoRoutes = require("./avaliacoes.routes");
const vagasRoutes = require("./vagas.routes");

const router = Router();

router.use("/tipos-de-usuarios", tiposDeUsuariosRoutes);
router.use("/areas-de-atuacao", areasAtuacaoRoutes);
router.use("/usuarios", usuariosRoutes);
router.use("/plantoes", plantoesRoutes);
router.use("/candidatos", candidatosRoutes);
router.use("/avaliacoes", avaliacaoRoutes);
router.use("/vagas", vagasRoutes);


module.exports = router;