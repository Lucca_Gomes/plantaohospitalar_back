const express = require("express");
const tiposDeUsuariosRoutes = express.Router();
const tipoUsuarioController = require("../controllers/tipoUsuarioController");

tiposDeUsuariosRoutes.get("/", (req, res) => {
    tipoUsuarioController.listaTipoUsuario(req, res);
});

tiposDeUsuariosRoutes.get("/:id", (req, res) => {
    tipoUsuarioController.idTipoUsuario(req, res);
});

module.exports = tiposDeUsuariosRoutes;
