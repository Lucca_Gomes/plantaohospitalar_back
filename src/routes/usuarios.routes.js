const express = require("express");
const usuariosRoutes = express.Router();

const getUserInPath = require("../middlewares/getUserIdInPathMiddleware");
const checkEmptyBodyRequest = require("../middlewares/checkEmptyBodyRequestMiddleware");
const checkCadastroBodyRequest = require("../middlewares/checkCadastroBodyRequestMiddleware");
const checkAdminAuth = require("../middlewares/checkAdminAuthMiddleware");

const usuariosController = require("../controllers/usuariosController");

usuariosRoutes.get("/", checkAdminAuth, (req, res) => usuariosController.listaUsuarios(req, res));
usuariosRoutes.post("/", checkAdminAuth, checkEmptyBodyRequest, checkCadastroBodyRequest, (req, res) => usuariosController.criaUsuarios(req, res));

usuariosRoutes.get("/:id", getUserInPath, (req, res) => usuariosController.getUsuarioById(req, res));
usuariosRoutes.patch("/:id", checkAdminAuth, getUserInPath, checkEmptyBodyRequest, (req, res) => usuariosController.patchUsuarioById(req, res));
usuariosRoutes.delete("/:id", checkAdminAuth, getUserInPath, (req, res) => usuariosController.deleteUsuarioById(req, res));


usuariosRoutes.get("/sessao/login", (req, res) => usuariosController.loginUsuario(req, res));
usuariosRoutes.post("/sessao/login", checkEmptyBodyRequest, checkCadastroBodyRequest, (req, res) => usuariosController.cadastroUsuario(req, res));

usuariosRoutes.get("/sessao/usuario", (req, res) => usuariosController.sessaoUsuario(req, res));

module.exports = usuariosRoutes;
