const { AreaAtuacao } = require('../models/models')

const Erro = require("../exceptions/Erro");


async function createareaAtuacao (req, res) {
    try {
        const { areaDeAtuacao } = req.body
        const areaAtuacao = await AreaAtuacao.create({
            areaDeAtuacao
        })

        return res.status(201).json(areaAtuacao)
    
    } catch (error) {
        return res.status(500).json(new Erro({ descricao: "Ocorreu um erro ao tentar criar uma áreas de atuação." }));
    }
}

async function retornaLista (req, res) {
    try {
        const areasAtuacao = await AreaAtuacao.findAll()
        return res.status(200).json(areasAtuacao)

    } catch (error) {
        console.log(error);

        return res.status(500).json(new Erro({ descricao: "Ocorreu um erro ao tentar listar as áreas de atuação." }));
    }
}

async function retornaId (req, res) {
    try {
        const { id } = req.params
        const areaAtuacao = await AreaAtuacao.findOne({
        
            where: {
                id
            }
        
        })

        return res.status(200).json(areaAtuacao)


    } catch (error) {
        return res.status(500).json(new Erro({ descricao: "Ocorreu um erro ao tentar encontrar uma área de atuação pelo seu id." }));
    }
}




module.exports = {
    createareaAtuacao,
    retornaLista,
    retornaId,
}

