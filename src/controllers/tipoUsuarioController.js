const { TipoUsuario } = require("../models/models");

const Erro = require("../exceptions/Erro");
const erroBancoDeDados = require("../exceptions/ErroBancoDeDados");


async function listaTipoUsuario(req, res){
    try {
        const tipos = await TipoUsuario.findAll();
        return res.status(200).json(tipos);
    } catch (error) {
        return res.status(500).json(new Erro({ descricao: "Ocorreu um erro ao tentar listar todos os tipos de usuário." }));
    }
}

async function idTipoUsuario(req, res){
    try {
        const { id } = req.params;

        const idTipo = await TipoUsuario.findOne({
            where: {
                id: id,
            },
        });
        
        if (idTipo) {
            return res.status(200).json(idTipo);
        }

        return res.status(404).json(
            new erroBancoDeDados.ErroNaoEncontrado({ tabela: "TipoUsuarios" })
        );

    } catch (error) {
        return res.status(400).json(
            new Erro({ descricao: "Ocorreu um erro ao tentar encontrar o tipo de usuário pelo seu id."} )
        );
    }
}

module.exports = {
    listaTipoUsuario,
    idTipoUsuario
}