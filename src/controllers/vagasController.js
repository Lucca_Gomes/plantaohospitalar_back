const Vaga = require("../models/vaga")

async function criarVaga(req, res) {
    try {
        const dados = req.dados;
        const vaga = await Vaga.create(dados);
        return res.status(201).json(vaga);
    } catch (error) {
        return res.status(400).json({
            erro: "erro",
            erroTipo: "erro",
            erroDescricao: "Ocorreu um erro ao tentar criar a vaga!"
        })
    }
}

async function listarVagas(req, res) {
    try {
        const vagas = await Vaga.findAll();
        return res.status(200).json(vagas);
    } catch (error) {
        return res.status(400).json({
            erro: "erroBancoDeDados",
            erroTipo: "erro",
            erroDescricao: "Ocorreu um erro ao tentar listar as vagas!"
        });
    }
}

async function retornaVaga(req, res) {
    try {
        const vaga = req.vaga;
        return res.status(200).json(vaga);
    } catch (error) {
        return res.status(400).json({
            erro: "erro",
            erroTipo: "erro",
            erroDescricao: "Ocorreu uma falha ao retornar a vaga!"
        });
    }
}

async function atualizaVaga(req, res) {
    try {
        const vaga = req.vaga;
        const dados = req.body;
        await vaga.update(dados);
        return res.status(200).json(vaga);
    } catch (error) {
        return res.status(400).json({
            erro: "erro",
            erroTipo: "erro",
            erroDescricao: "Ocorreu um erro ao tentar atualizar as informações da vaga!"
        });
    }
}


async function excluiVaga(req, res) {
    try {
        const vaga = req.vaga;
        await vaga.destroy();
        return res.status(200).json("Vaga deletada");
    } catch (error) {
        return res.status(400).json({
            erro: "erro",
            erroTipo: "erro",
            erroDescricao: "Ocorreu uma falha ao tentar deletar a vaga!"
        });
    }
}

async function retornaVagasHospital(req, res) {
    try {
        const { usuarioHospital_id } = req.params;
        const vagas = await Vaga.findAll({where: {usuarioHospital_id}});
        return res.status(200).json(vagas);
    } catch (error) {
        return res.status(400).json({
            erro: "erro",
            erroTipo: "erro",
            erroDescricao: "Ocorreu um erro ao retornar as vagas por Hospital!"
        });
    }
}

async function retornaVagasArea(req, res) {
    try {
        const { area } = req.params;
        const vagas = await Vaga.findAll({where: {area}});
        return res.status(200).json(vagas);
    } catch (error) {
        return res.status(400).json({
            erro: "erro",
            erroTipo: "erro",
            erroDescricao: "Ocorreu uma falha ao retornar as vagas por Área!"
        });
    }
}

module.exports = {
    criarVaga,
    listarVagas,
    retornaVaga,
    atualizaVaga,
    excluiVaga,
    retornaVagasHospital,
    retornaVagasArea,
}