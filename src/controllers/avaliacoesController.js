const { Avaliacao } = require('../models/models')
const Erro = require("../exceptions/Erro");
const { ErroPropriedadeObrigatoria } = require('../exceptions/ErroCorpoDeRequisicao');

// Lista todas as avaliações disponíveis
async function listaAvaliacoes (req, res){
    try {
        const listAval = await Avaliacao.findAll();
        return res.status(200).json(listAval);
    } catch (error){
        return res.status(500).json(new Erro({ descricao: "Ocorreu um erro ao tentar listar todas as avaliações." }));
    }
}

//Submete nova avaliação
async function enviaAvaliacao(req, res){
    try {
        if(!req.body.comentario){
            return res.status(400).json(new ErroPropriedadeObrigatoria({ propNome: "comentario" }));
        }
        const novaAvaliacao = await Avaliacao.create({
            nota: req.body.nota,
            comentario: req.body.comentario,
        })
        return res.status(201).json(novaAvaliacao);
    } catch (error) {
        console.log(error);
        return res.status(500).json(new Erro({ descricao: "Ocorreu um erro ao tentar submeter a avaliação." }));
    }
}

//Busca avaliação por ID
async function avaliacaoId(req, res){
    try {
        const { id } = req.params;

        const idAval = await Avaliacao.findOne({
            where:{
                id: id,
            },
        });
        if(!idAval){
            return res.status(404).json(new Erro({ descricao: "Avaliação não encontrada" }));
        }
        return res.status(200).json(idAval);
    } catch (error) {
        return res.status(500).json(new Erro({ descricao: "Ocorreu um erro" }));
    }
}

// avaliacoes/de/{usuarioId} //
async function avaliacoesDeUser (req, res) {
    try {
        const { usuarioId } = req.params
        const avaliacao = await Avaliacao.findAll({
            where:{
                avaliadorId: usuarioId
            }
        });

        if(!avaliacao) {
            return res.status(404).json({error: "Usuário não encontrado!"})
        }
        return res.status(200).json(avaliacao)
    } catch (error) {
        
        return res.status(500).json(error)
        
    }
}


//avaliacoes/para/{usuarioId}//
async function avaliacoesParaUser (req, res) {
    try {
        const { usuarioId } = req.params
        const avaliacao = await Avaliacao.findAll({
            where:{
                avaliadoId: usuarioId
            }
        });

        if(!avaliacao) {
            return res.status(404).json({error: "Usuário não encontrado!"})
        }
        return res.status(200).json(avaliacao)
    } catch (error) {
        return res.status(500).json(error)
    }
}


//avaliacoes/media/{usuarioId}//
async function avaliacoesMedia (req, res) {
    try {
        const { usuarioId } = req.params
        const avaliacoes = await Avaliacao.findAll({
            where:{
                avaliadoId: usuarioId
            }
        });

        if(!avaliacoes) {
            return res.status(404).json({error: "Usuário não encontrado!"})
        }

        let soma = 0

        for (const av of avaliacoes){
            soma+=av.nota
        }

        let media = soma/avaliacoes.length


        return res.status(200).json(media)
    } catch (error) {
        return res.status(500).json(error)
    }
}

module.exports = {
    listaAvaliacoes,
    enviaAvaliacao,
    avaliacaoId,
    avaliacoesDeUser, 
    avaliacoesParaUser,
    avaliacoesMedia
}
