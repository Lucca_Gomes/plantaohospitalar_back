const { Candidato, Vaga } = require("../models/models");

const Erro = require("../exceptions/Erro");
const erroBancoDeDados = require("../exceptions/ErroBancoDeDados");
const erroCorpoDeRequisicao = require("../exceptions/ErroCorpoDeRequisicao");

async function listaCandidatos(req, res) {
    try {
        let usuarios = await Candidato.findAll();

        return res.status(200).json(usuarios);
    } catch (error) {
        return res.status(500).json(
            new erroBancoDeDados.ErroBancoDeDados({ descricao: "Ocorreu uma falha ao tentar listar os usuários."} )
        );
    }
}

async function criaCandidato(req, res) {
    try {
        const dados = {};
        
        for (let prop of [ "candidatoId", "vagaId" ]) {
            if (req.body[prop] == null) {
                return res.status(400).json(
                    new erroCorpoDeRequisicao.ErroPropriedadeObrigatoria({ propNome: prop })
                )
            }

            dados[prop] = req.body[prop];
        }

        const candidato = await Candidato.create(dados);

        return res.status(201).json(candidato);

    } catch (error) {
        switch (error.name) {
            case "SequelizeForeignKeyConstraintError":
                return res.status(400).json(
                    new erroBancoDeDados.ErroBancoDeDados({ descricao: "Foreign key fornecida não reconhecida." })
                );

            case _:
                return res.status(500).json(
                    new Erro({ descricao: "Ocorreu uma falha ao tentar criar um novo candidato." })
                );
        }
    }
}

async function getCandidatoById(req, res) {
    try {
        const { id } = req.params;

        const candidato = await Candidato.findOne({
            where: {
                id
            }
        })

        if (candidato == null) {
            return res.status(404).json(
                new erroBancoDeDados.ErroNaoEncontrado({ tabela: "Candidatos" })
            )
        }

        return res.status(200).json(candidato);

    } catch (error) {
        return res.status(500).json(
            new Erro({ descricao: "Ocorreu uma falha ao encontrar um candidato pelo seu id." })
        );
    }
}

async function getCandidatosByMedico(req, res) {
    try {
        const candidaturas = await Candidato.findAll({
            where: {
                candidatoId: req.usuario.id
            }
        });

        return res.status(200).json(candidaturas);

    } catch (error) {
        return res.status(500).json(
            new Erro({ descricao: "Ocorreu uma falha ao tentar encontrar as candidaturas de um médico." })
        );
    }
}

async function getCandidatosByVaga(req, res) {
    try {
        const { id } = req.params;

        const vaga = await Vaga.findOne({
            where: {
                id
            }
        })

        if (vaga == null) {
            return res.status(404).json(
                new erroBancoDeDados.ErroNaoEncontrado({ tabela: "Vagas" })
            )
        }

        const candidatos = await Candidato.findAll({
            where: {
                vagaId: vaga.id
            }
        });

        return res.status(200).json(candidatos);

    } catch (error) {
        return res.status(500).json(
            new Erro({ descricao: "Ocorreu uma falha ao tentar encontrar os candidatos de uma vaga." })
        );
    }
}


module.exports = {
    listaCandidatos,
    criaCandidato,
    getCandidatoById,
    getCandidatosByMedico,
    getCandidatosByVaga
}
