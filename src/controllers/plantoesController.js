const { Plantao } = require("../models/models");
const { Op } = require("sequelize");

const Erro = require("../exceptions/Erro");
const erroCorpoDeRequisicao = require("../exceptions/ErroCorpoDeRequisicao");


async function generateAutorizacaoCheckInId(plantoes=null) {
    if (!plantoes) {
        plantoes = await Plantao.findAll();
    }

    let idInteger = 0;
    for (let plantao of plantoes) {
        let plantao_id = parseInt(plantao.autorizacaoCheckIn);
        if (plantao_id > idInteger) {
            idInteger = plantao_id;
        }
    }
    idInteger++;

    return String(idInteger).padStart(6, "0");
}

async function generateAutorizacaoCheckOutId(plantoes=null) {
    if (!plantoes) {
        plantoes = await Plantao.findAll();
    }

    let idInteger = 0;
    for (let plantao of plantoes) {
        let plantao_id = parseInt(plantao.autorizacaoCheckOut);
        if (plantao_id > idInteger) {
            idInteger = plantao_id;
        }
    }
    idInteger++;

    return String(idInteger).padStart(6, "0");
}


async function listaPlantoes(req, res) {
    try {
        const { disponivelPara } = req.query;

        let filter;

        for (let status of [ "check-in", "check-out", "historico" ]) {
            if (disponivelPara == status) {
                filter = status;
            }
        }

        let plantoes;

        if (filter) {
            plantoes = await Plantao.findAll({ where: { disponivelPara: filter } });
        } else {
            plantoes = await Plantao.findAll();
        }

        return res.status(200).json(plantoes);

    } catch (error) {
        return res.status(500).json(new Erro({ descricao: "Ocorreu um erro ao tentar listar os plantões." }));
    }
}

async function criaPlantao(req, res) {
    try {
        const dados = {};

        for (let prop of ["vagaId", "hospitalId", "medicoId"]) {
            if (req.body[prop] == null) {
                return res.status(400).json(
                    new erroCorpoDeRequisicao.ErroPropriedadeObrigatoria({ propNome: prop })
                )
            }

            dados[prop] = req.body[prop];
        }

        const plantoes = await Plantao.findAll();
        dados.autorizacaoCheckIn = await generateAutorizacaoCheckInId(plantoes);
        dados.autorizacaoCheckOut = await generateAutorizacaoCheckOutId(plantoes);

        const plantao = await Plantao.create(dados);

        return res.status(201).json(plantao);

    } catch (error) {
        console.log(error);
        return res.status(500).json(new Erro({ descricao: "Ocorreu um erro ao tentar criar um plantão." }))
    }
}

async function getPlantaoById(req, res) {
    return res.status(200).json(req.plantao);
}

async function patchPlantaoById(req, res) {
    try {
        const dados = req.body;

        await req.plantao.update(dados);

        return res.status(200).json(req.plantao);

    } catch (error) {
        return res.status(500).json(
            new Erro({ descricao: "Ocorreu uma falha ao tentar atualizar as informações de um plantão." })
        );
    }
}

async function deletePlantaoById(req, res) {
    try {
        await req.plantao.destroy();

        return res.status(200).json({ descricao: "O plantão foi deletado com sucesso." });
        
    } catch (error) {
        return res.status(500).json(
            new Erro({ descricao: "Ocorreu uma falha ao tentar deletar um plantão." })
        );
    }
}

async function getPlantoesByUser(req, res) {
    try {
        const { disponivelPara } = req.query;

        let filter;
        for (let status of [ "check-in", "check-out", "historico" ]) {
            if (disponivelPara == status) {
                filter = status;
            }
        }

        let plantoes;
        if (filter) {
            plantoes = await Plantao.findAll({
                where: {
                    disponivelPara: filter,
                    [Op.or]: [
                        { medicoId: req.usuario.id },
                        { hospitalId: req.usuario.id }
                    ]
                }
            });
        } else {
            plantoes = await Plantao.findAll({
                where: {
                    [Op.or]: [
                        { medicoId: req.usuario.id },
                        { hospitalId: req.usuario.id }
                    ]
                }
            });
        }

        return res.status(200).json(plantoes);

    } catch (error) {
        return res.status(500).json(
            new Erro({ descricao: "Ocorreu uma falha ao tentar encontrar plantões por um usuário." })
        );
    }
}

module.exports = {
    listaPlantoes,
    criaPlantao,
    getPlantaoById,
    patchPlantaoById,
    deletePlantaoById,
    getPlantoesByUser
}
