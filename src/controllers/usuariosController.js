const { Usuario } = require("../models/models");
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { v4: uuidv4 } = require("uuid")

const Erro = require("../exceptions/Erro");
const erroParametroDeRequisicao = require("../exceptions/ErroParametroDeRequisicao");
const erroBancoDeDados = require("../exceptions/ErroBancoDeDados");
const erroSessao = require("../exceptions/ErroSessao");


async function generateId(usuarios=null) {
    if (!usuarios) {
        usuarios = await Usuario.findAll();
    }

    let idInteger = 0;
    for (let usuario of usuarios) {
        let usuario_id = parseInt(usuario.id);
        if (usuario_id > idInteger) {
            idInteger = usuario_id;
        }
    }
    idInteger++;
    return String(idInteger).padStart(4, "0");
}

async function listaUsuarios(req, res) {
    try {
        let usuarios = await Usuario.findAll();

        return res.status(200).json(usuarios);
    } catch (error) {
        return res.status(500).json(
            new erroBancoDeDados.ErroBancoDeDados({ descricao: "Ocorreu uma falha ao tentar listar os usuários."} )
        );
    }
}

async function criaUsuarios(req, res) {
    try {
        const dados = req.dados;

        dados.status = true;
        dados.senha = await bcrypt.hash(dados.senha, 8);
        dados.id = await generateId();

        const usuario = await Usuario.create(dados);

        return res.status(201).json(usuario);

    } catch ( error ) {
        switch (error.name) {
            case "SequelizeForeignKeyConstraintError":
                return res.status(400).json(
                    new erroBancoDeDados.ErroBancoDeDados({ descricao: "Foreign key fornecida não reconhecida." })
                );

            case _:
                return res.status(500).json(
                    new Erro({ descricao: "Ocorreu uma falha ao tentar criar um novo usuário." })
                );
        }
    }
}

async function getUsuarioById(req, res) {
    return res.status(200).json(req.usuario);
}

async function patchUsuarioById(req, res) {
    try {
        const dados = req.body;

        if (dados.senha) {
            dados.senha = await bcrypt.hash(dados.senha, 8);
        }

        await req.usuario.update(dados);

        return res.status(200).json(req.usuario);

    } catch (error) {
        return res.status(500).json(
            new Erro({ descricao: "Ocorreu uma falha ao tentar atualizar as informações de um usuário." })
        );
    }
}

async function deleteUsuarioById(req, res) {
    try {
        await req.usuario.destroy();

        return res.status(200).json({ descricao: "O usuário foi deletado com sucesso." });
        
    } catch (error) {
        return res.status(500).json(
            new Erro({ descricao: "Ocorreu uma falha ao tentar deletar um usuário." })
        );
    }
}

async function loginUsuario(req, res) {
    try {
        const { email, senha } = req.query;

        const usuario = await Usuario.findOne({
            where: {
                email
            }
        });

        if (usuario == null) {
            return res.status(401).json(new erroSessao.ErroLoginInvalido({ email, senha }))
        }

        if (!usuario.status) {
            return res.status(401).json(new erroSessao.ErroUsuarioNaoAprovado());
        }

        if(!(await bcrypt.compare(senha, usuario.senha)) && senha != usuario.senha) {
            return res.status(401).json(new erroSessao.ErroLoginInvalido({ email, senha }))
        }

        const secretKey = uuidv4();
        const token = jwt.sign({id: usuario.id}, secretKey, {expiresIn:60*60});

        return res.status(200).json({
            token,
            secretKey
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json(new Erro({ descricao: "Ocorreu uma falha ao tentar fazer o login de um usuário." }));
    }
}

async function cadastroUsuario(req, res) {
    try {
        const dados = req.dados;

        dados.status = false;
        dados.senha = await bcrypt.hash(dados.senha, 8);
        dados.id = await generateId();

        const usuario = await Usuario.create(dados);

        return res.status(201).json(usuario);

    } catch ( error ) {
        return res.status(500).json(new Erro({ descricao: "Ocorreu uma falha ao tentar cadastrar um novo usuário." }))
    }
}

async function sessaoUsuario(req, res) {
    try {
        const { token, secretKey } = req.query;

        if (token == null) {
            return res.status(401).json(
                new erroParametroDeRequisicao.ErroParametroObrigatorio({
                    paramNome: "token",
                    paramIn: "query"
                })
            )
        }

        if (secretKey == null) {
            return res.status(401).json(
                new erroParametroDeRequisicao.ErroParametroObrigatorio({
                    paramNome: "secretKey",
                    paramIn: "query"
                })
            )
        }

        const decoded = jwt.verify(token, secretKey);
        const usuarioId = decoded.id;

        const usuario = await Usuario.findByPk(usuarioId);

        if (usuario == null) {
            return res.status(401).json(
                new erroSessao.ErroTokenAssinaturaInvalida()
            )
        }

        return res.status(200).json(usuario);

    } catch (error) {
        switch (error.name) {
            case "TokenExpiredError":
                return res.status(401).json(
                    new erroSessao.ErroTokenExpirado()
                )
            case "JsonWebTokenError":
                return res.status(401).json(
                    new erroSessao.ErroTokenAssinaturaInvalida()
                )
        }

        return res.status(500).json(
            new Erro({ descricao: "Ocorreu uma falha ao tentar ler a sessão." })
        );
    }
}

module.exports = {
    listaUsuarios,
    criaUsuarios,
    getUsuarioById,
    patchUsuarioById,
    deleteUsuarioById,
    loginUsuario,
    cadastroUsuario,
    sessaoUsuario
}
