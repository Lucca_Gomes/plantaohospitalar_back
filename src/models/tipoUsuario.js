const sequelize = require("../../config/database.js");
const { DataTypes } = require("sequelize");

const TipoUsuario = sequelize.define('TipoUsuario', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    allowNull: false,
  },
  tipo: {
    type: DataTypes.STRING,
    allowNull: false,
  },
})

module.exports = TipoUsuario;
