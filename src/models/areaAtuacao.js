const sequelize = require("../../config/database.js");
const { DataTypes, STRING } = require("sequelize");
const { v4 : UUIDV4 } = require('uuid')

const AreaAtuacao = sequelize.define('areaAtuacao', {
    id: {
        type: DataTypes.UUIDV4 , 
        primaryKey: true, 
        defaultValue: () => UUIDV4()

    },

    areaDeAtuacao: {
        type: STRING, 
        allowNull: false
    }
})


module.exports = AreaAtuacao;