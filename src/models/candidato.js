const sequelize = require("../../config/database.js");
const { DataTypes } = require("sequelize");
const { v4: uuidv4 } = require("uuid");

const Candidato = sequelize.define('Candidato', {
    id: {
        type: DataTypes.UUID,
        defaultValue: () => uuidv4(),
        primaryKey: true,
        allowNull: false,
    }
})

module.exports = Candidato;