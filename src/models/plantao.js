const sequelize = require("../../config/database.js");
const { DataTypes } = require("sequelize");
const { v4: uuidv4 } = require("uuid");

const Plantao = sequelize.define('Plantao', {
  id: {
    type: DataTypes.UUID,
    defaultValue: () => uuidv4(),
    primaryKey: true,
    allowNull: false,
  },
  disponivelPara: {
    type: DataTypes.STRING,
    allowNull: false,
    defaultValue: "check-in"
  },
  autorizacaoCheckIn: {
    type: DataTypes.STRING,
    allowNull: false
  },
  autorizacaoCheckOut: {
    type: DataTypes.STRING,
    allowNull: false
  },
})

module.exports = Plantao;
