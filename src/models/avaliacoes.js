const sequelize = require("../../config/database.js");
const { DataTypes, STRING } = require("sequelize");
const { v4 : UUIDV4 } = require('uuid')

const avaliacoes = sequelize.define('avaliacoes', {
    id: {
        type: DataTypes.UUIDV4 , 
        primaryKey: true, 
        defaultValue: () => UUIDV4()

    },
    nota: {
        type: DataTypes.INTEGER ,
        allowNull: false
    },
    comentario: {
        type: STRING, 
        allowNull: false
    },
})


module.exports = avaliacoes;
