const TipoUsuario = require("./tipoUsuario");
const AreaAtuacao = require("./areaAtuacao");
const Usuario = require("./usuario");
const Plantao = require("./plantao");
const Candidato = require("./candidato");
const Avaliacao = require("./avaliacoes");
const Vaga = require("./vaga");

TipoUsuario.hasMany(Usuario, { foreignKey: "tipo" });

AreaAtuacao.hasMany(Usuario, { foreignKey: "areaDeAtuacaoId" });
AreaAtuacao.hasMany(Vaga, { foreignKey: "areaDeAtuacaoId" })

Usuario.belongsTo(TipoUsuario, { foreignKey: "tipo" });
Usuario.belongsTo(AreaAtuacao, { foreignKey: "areaDeAtuacaoId" });
Usuario.hasMany(Plantao, { foreignKey: "hospitalId" });
Usuario.hasMany(Plantao, { foreignKey: "medicoId" });
Usuario.hasMany(Candidato, { foreignKey: "candidatoId" });
Usuario.hasMany(Avaliacao,  { foreignKey: "avaliadorId" });
Usuario.hasMany(Avaliacao,  { foreignKey: "avaliadoId" });
Usuario.hasMany(Vaga, { foreignKey: "hospitalId" });

Plantao.belongsTo(Usuario, { foreignKey: "hospitalId" });
Plantao.belongsTo(Usuario, { foreignKey: "medicoId" });
Plantao.belongsTo(Vaga, { foreignKey: "vagaId" });

Candidato.belongsTo(Usuario, { foreignKey: "candidatoId" });
Candidato.belongsTo(Vaga, { foreignKey: "VagaId" });

Vaga.hasMany(Candidato, { foreignKey: "VagaId" });
Vaga.hasMany(Plantao, { foreignKey: "vagaId" });
Vaga.belongsTo(AreaAtuacao, { foreignKey: "areaDeAtuacaoId" });
Vaga.belongsTo(Usuario, { foreignKey: "hospitalId" });

Avaliacao.belongsTo(Usuario, { foreignKey: "avaliadorId" });
Avaliacao.belongsTo(Usuario, { foreignKey: "avaliadoId" });


module.exports = {
    TipoUsuario,
    AreaAtuacao,
    Usuario,
    Plantao,
    Candidato,
    Avaliacao
}
