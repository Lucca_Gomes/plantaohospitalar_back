const { DataTypes } = require("sequelize");
const sequelize = require("../../config/database");
const { v4: uuidv4 } = require("uuid");

const Vaga = sequelize.define("Vaga", {
    id: {
        type: DataTypes.UUIDV4,
        defaultValue: () => uuidv4(),
        primaryKey: true,
    },
    inicio: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    fim: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    valor: {
        type: DataTypes.FLOAT,
        allowNull: false,
    },
    ocupado: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
    },
});

module.exports = Vaga;